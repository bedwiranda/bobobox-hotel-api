﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class HotelUser
    {
        public HotelUser()
            : base()
        {
        }

        public HotelUser(Guid hotelId, Guid userId, bool isOwner = false)
            : this()
        {
            HotelId = hotelId;
            UserId = userId;
            IsOwner = isOwner;
        }


        [Key]
        [Column(Order = 1)]
        public Guid HotelId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid UserId { get; protected set; }

        public bool IsOwner { get; protected set; }

        //navigations
        public Hotel Hotel { get; set; }
        public User User { get; set; }
    }
}
