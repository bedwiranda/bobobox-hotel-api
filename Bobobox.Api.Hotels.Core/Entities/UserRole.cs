﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class UserRole
    {
        public UserRole()
            : base()
        {
        }

        public UserRole(Guid userId, Guid roleId)
            : this()
        {
            UserId = userId;
            RoleId = roleId;
        }


        [Key]
        [Column(Order = 1)]
        public Guid UserId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid RoleId { get; protected set; }

        //navigations
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
