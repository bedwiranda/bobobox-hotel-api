﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class Transaction
    {
        public Transaction()
            : base()
        {
        }

        public Transaction(Guid hotelId)
            : this()
        {
            HotelId = hotelId;
        }


        [Key]
        [Column(Order = 1)]
        public Guid HotelId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid Id { get; set; }

        public Guid ReservationId { get; set; }

        public decimal Amount { get; set; }

        public Guid PaymentMethodId { get; set; }

        public int Status { get; set; }

        //navigations
        public Hotel Hotel { get; set; }
        public Reservation Reservation { get; set; }
    }
}
