﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class Employee
    {
        private string _nationalPhoneNumber;
        private string _phoneNumber;

        public Employee()
            : base()
        {
        }


        [Key]
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FullName
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
            private set { }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string NationalPhoneNumber
        {
            get { return _nationalPhoneNumber; }
            set
            {
                _nationalPhoneNumber = (!string.IsNullOrWhiteSpace(value)) ? value : null;
            }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                _phoneNumber = (!string.IsNullOrWhiteSpace(value)) ? value : null;
            }
        }
    }
}
