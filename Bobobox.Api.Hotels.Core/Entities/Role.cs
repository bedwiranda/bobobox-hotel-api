﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class Role
    {
        public Role()
            : base()
        {
        }

        [Key]
        public Guid Id { get; set; }

        public string Name{ get; set; }

        //navigations
    }
}
