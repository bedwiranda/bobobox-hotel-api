﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class Reservation
    {
        public Reservation()
            : base()
        {
        }

        public Reservation(Guid hotelId)
            : this()
        {
            HotelId = hotelId;
        }


        [Key]
        [Column(Order = 1)]
        public Guid HotelId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid Id { get; set; }

        public Guid HotelRoomId { get; set; }

        public Guid CustomerId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int Status { get; set; }

        //navigations
        // public Hotel Hotel { get; set; }
        // public HotelRoom HotelRoom { get; set; }
        //public User Customer { get; set; }
    }
}
