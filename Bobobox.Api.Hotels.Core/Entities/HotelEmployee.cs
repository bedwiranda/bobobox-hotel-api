﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class HotelEmployee
    {
        public HotelEmployee()
            : base()
        {
        }

        public HotelEmployee(Guid hotelId)
            : this()
        {
            HotelId = hotelId;
        }


        [Key]
        [Column(Order = 1)]
        public Guid HotelId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid EmployeeId { get; set; }

        //navigations
        public Hotel Hotel { get; set; }
        public Employee Employee{ get; set; }
    }
}
