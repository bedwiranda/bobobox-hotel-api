﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Entities
{
    public class Hotel
    {
        private string _email;

        public Hotel()
        {

        }

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email
        {
            get { return _email; }
            set { _email = (!string.IsNullOrWhiteSpace(value)) ? value.Trim().ToLower() : null; }
        }

        public string TimeZoneId { get; set; }

        public Guid? OwnerUserId { get; set; }

        public DateTime JoinDateUtc { get; set; }

        /// <summary>
        /// Id of user that create record
        /// </summary>
        public Guid? CreateUserId { get; set; }

        /// <summary>
        /// Id of user that edit record
        /// </summary>
        public Guid? LastEditUserId { get; set; }

        /// <summary>
        /// Last Edit Date (in Utc)
        /// </summary>
        public DateTime? LastEditDateUtc { get; set; }

        public byte[] RowVersion { get; set; }

        // navigations
        public User OwnerUser { get; set; }
        public User CreateUser { get; set; }
        public User LastEditUser { get; set; }

    }
}
