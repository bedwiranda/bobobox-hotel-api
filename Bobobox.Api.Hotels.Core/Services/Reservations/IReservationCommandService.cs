﻿using Bobobox.Api.Hotels.Core.Entities;
using Bobobox.Api.Hotels.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Cores.Services.Reservations
{
    public interface IReservationCommandService
    {
        Task<ServiceResult<Reservation>> CreateReservationAsync(object dto);
    }
}
