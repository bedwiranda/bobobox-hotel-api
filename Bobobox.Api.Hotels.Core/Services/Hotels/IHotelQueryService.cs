﻿using Bobobox.Api.Hotels.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Services.Hotels
{
    public interface IHotelQueryService
    {
        Task<List<Hotel>> GetHotelAsync();
    }
}
