﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Core.Entities;
using Bobobox.Api.Hotels.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Cores.Services.Sales
{
    public interface IHotelCommandService
    {
        Task<ServiceResult<Hotel>> AddHotelAsync(HotelCreateDTO dto);
        Task<ServiceResult<Hotel>> UpdateHotelAsync(Guid hotelId, HotelUpdateDTO dto);
    }
}
