﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Core.Shares;
using Bobobox.Repository.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Services.Hotels
{
    public interface ICarCommandService
    {
        Task<ServiceResult<Car>> AddCarAsync(CarCreateDTO dto);

        Task<ServiceResult<Car>> UpdateCarAsync(int id, CarUpdateDTO dto);
    }
}
