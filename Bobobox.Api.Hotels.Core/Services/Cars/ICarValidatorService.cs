﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Repository.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Services.Hotels
{
    public interface ICarValidatorService
    {
        Task<bool> ValidateCarCreateAsync(ICarRepository repo, CarCreateDTO dto);

        Task<bool> ValidateCarUpdateAsync(ICarRepository repo, Car car, CarUpdateDTO dto);
    }
}
