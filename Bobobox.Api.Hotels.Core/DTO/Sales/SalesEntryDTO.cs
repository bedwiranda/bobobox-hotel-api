﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.DTO.Sales
{
    public class SalesEntryDTO
    {
        public ICollection<int> CarIds { get; set; }
    }
}
