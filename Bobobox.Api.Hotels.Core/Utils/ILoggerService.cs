﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Utils
{
    public interface ILoggerService
    {
        void LogInfo(string msg, string category = null, string subcategory = null);

        void LogWarning(string msg, string category = null, string subcategory = null, Exception exception = null);

        void LogError(Exception exception, string msg, string category = null, string subcategory = null);
    }
}
