﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Core.Shares
{
    public interface IInjectorService
    {
        T GetInstance<T>() where T : class;
    }
}
