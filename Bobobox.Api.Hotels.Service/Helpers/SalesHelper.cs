﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Service.Helpers
{
    public class SalesHelper
    {
        public static decimal CalculateDiscountBefore2000(decimal sumPrice, int carYear, decimal carPrice)
        {
            var discountLimitYear = 2000;

            if (carYear < discountLimitYear)
            {
                sumPrice = sumPrice + carPrice - (carPrice * 10 / 100);
            }
            else
            {
                sumPrice = sumPrice + carPrice;
            }

            return sumPrice;
        }

        public static decimal CalculateDiscountBasedOnNumberOfHotels(decimal sumPrice, ICollection<int> carIds)
        {
            var numOfHotelsToBuy = carIds.Count();
            if (numOfHotelsToBuy > 2)
            {
                sumPrice = sumPrice - (sumPrice * 3 / 100);
            }

            return sumPrice;
        }

        public static decimal CalculateDiscountBasedOnTotalCost(decimal sumPrice)
        {
            var limitDiscount5 = 100000;
            if (sumPrice > limitDiscount5)
            {
                sumPrice = sumPrice - (sumPrice * 5 / 100);
            }
            return sumPrice;
        }
    }
}
