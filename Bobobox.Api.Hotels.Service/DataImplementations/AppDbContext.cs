﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.Entity;
using Bobobox.Api.Hotels.Core.DataInterfaces;
using Bobobox.Api.Hotels.Core.Entities;

namespace Bobobox.Api.Hotels.Service.DataImplementations
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext()
            : base("Name=BoboboxContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            this.Database.CommandTimeout = 120;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Hotel>().ToTable("Hotel");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<UserRole>().ToTable("UserRole");
            modelBuilder.Entity<Employee>().ToTable("Employee");
            modelBuilder.Entity<HotelRoom>().ToTable("HotelRoom");
            modelBuilder.Entity<HotelEmployee>().ToTable("HotelEmployee");
            modelBuilder.Entity<Reservation>().ToTable("Reservation");
            modelBuilder.Entity<Transaction>().ToTable("Transaction");
        }
    }
}
