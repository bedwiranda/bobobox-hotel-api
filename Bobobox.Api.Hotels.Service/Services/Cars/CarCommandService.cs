﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Core.Services.Hotels;
using Bobobox.Api.Hotels.Core.Shares;
using Bobobox.Api.Hotels.Core.Utils;
using Bobobox.Api.Hotels.Service.Services.Hotels;
using Bobobox.Repository.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Service.Services.Hotels
{
    public class CarCommandService : ICarCommandService
    {
        public ICarValidatorService _carValidator;
        public ICarRepository _carRepository;
        private readonly ILoggerService _loggerService;

        public CarCommandService(ICarValidatorService carValidator, ICarRepository carRepository, ILoggerService loggerService)
            : base()
        {
            _carValidator = carValidator;
            _carRepository = carRepository;
            _loggerService = loggerService;
        }

        public async Task<ServiceResult<Car>> AddCarAsync(CarCreateDTO dto)
        {
            var failResult = new ServiceResult<Car>();

            var validationResult =
                await _carValidator.ValidateCarCreateAsync(_carRepository, dto);
            if (!validationResult) return failResult;

            var car = new Car();
            //car.Id = we assume id is auto incremented by the repository, or use random integer
            car.Id = 1;
            car.Model = dto.Model;
            car.Make = dto.Make;
            car.Price = dto.Price;
            car.Year = dto.Year;
            car.CountryManufactured = dto.CountryManufactured;

            //save the data with async
            try
            {
                await _carRepository.Add(car);
            }
            catch (Exception e)
            {
                _loggerService.LogInfo(
                            $@"Failed to save data" + e.Message + "" + e.StackTrace + "",
                            category: "Car", subcategory: "Create");
            }

            //return
            return new ServiceResult<Car>(true, car);

        }

        public async Task<ServiceResult<Car>> UpdateCarAsync(int id, CarUpdateDTO dto)
        {
            var failResult = new ServiceResult<Car>();

            var cars = await _carRepository.GetAllHotels();
            var car = cars.Where(x => x.Id == id).FirstOrDefault();

            if (car == null)
                throw new ArgumentException("data is not found");

            var validationResult =
                await _carValidator.ValidateCarUpdateAsync(_carRepository, car, dto);
            if (!validationResult) return failResult;
            
            //update the data here
            car.Model = dto.Model;
            car.Make = dto.Make;
            car.Price = dto.Price;
            car.Year = dto.Year;
            car.CountryManufactured = dto.CountryManufactured;

            //save the data with async
            try
            {
                await _carRepository.Update(car);
            }
            catch (Exception e)
            {
                _loggerService.LogInfo(
                            $@"Failed to update data" + e.Message + "" + e.StackTrace + "",
                            category: "Car", subcategory: "Update");
            }

            //return
            return new ServiceResult<Car>(true, car);
        }
    }
}
