﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Core.Services.Hotels;
using Bobobox.Repository.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Service.Services.Hotels
{
    public class CarValidatorService : ICarValidatorService
    {
        public async Task<bool> ValidateCarCreateAsync(ICarRepository repo, CarCreateDTO dto)
        {
            //we can add validation here
            if (dto.Price < 0) return false;

            return true;
        }

        public async Task<bool> ValidateCarUpdateAsync(ICarRepository repo, Car car, CarUpdateDTO dto)
        {
            //we can add validation here
            if (dto.Price < 0) return false;

            return true;
        }
    }
}
