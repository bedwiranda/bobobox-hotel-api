﻿using Bobobox.Api.Hotels.Core.Entities;
using Bobobox.Api.Hotels.Core.Shares;
using Bobobox.Api.Hotels.Core.Utils;
using Bobobox.Api.Hotels.Cores.Services.Reservations;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using Bobobox.Api.Hotels.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Service.Services.Reservations
{
    public class ReservationCommandService : IReservationCommandService
    {
        public IReservationValidatorService _salesValidator;
        private readonly ILoggerService _loggerService;

        public ReservationCommandService(IReservationValidatorService salesValidator, ILoggerService loggerService)
            : base()
        {
            _salesValidator = salesValidator;
            _loggerService = loggerService;
        }

        public async Task<ServiceResult<Reservation>> CreateReservationAsync(object dto)
        {
            var failResult = new ServiceResult<Reservation>();

            return failResult;
        }
    }
}
