﻿using Bobobox.Api.Hotels.Core.DataInterfaces;
using Bobobox.Api.Hotels.Core.Entities;
using Bobobox.Api.Hotels.Core.Services.Hotels;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Service.Services.Hotels
{
    public class HotelQueryService : IHotelQueryService
    {
        public IAppDbContext _context;
        public IHotelValidatorService _hotelValidator;

        public HotelQueryService(IAppDbContext context, IHotelValidatorService hotelValidator)
            : base()
        {
            _context = context;
            _hotelValidator = hotelValidator;
        }

        public async Task<List<Hotel>> GetHotelAsync()
        {
            var hotelContext = _context.Set<Hotel>();

            return await hotelContext.ToListAsync();
        }
    }
}
