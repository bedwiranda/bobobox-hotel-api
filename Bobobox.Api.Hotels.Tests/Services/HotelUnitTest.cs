﻿using Bobobox.Api.Hotels.Core.DataInterfaces;
using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Core.Utils;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using Bobobox.Api.Hotels.Service.Services.Hotels;
using Bobobox.Api.Hotels.Service.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Tests.Services
{
    [TestFixture]
    public class HotelUnitTest
    {
        //initializer
        private static IHotelValidatorService _hotelValidator;
        private static IHotelCommandService _hotelService;
        private static ILoggerService _loggerService;
        private static IAppDbContext _context;

        [OneTimeSetUp]
        public void setUp()
        {
            _hotelValidator = new HotelValidatorService();
            _loggerService = new LoggerService();
        }

        #region HELPERS

        private static HotelCommandService CreateHotelCommandService()
        {

            var hotelCommandService = new HotelCommandService( _context, _hotelValidator, _loggerService);

            return hotelCommandService;
        }

        #endregion HELPERS

        [Test]
        public async Task CreateHotelAsync_GivenValidData_ReturnServiceResultHotel()
        {
            //arrange
            var hotelNew = new HotelCreateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };

            //create command service
            var hotelCommandService = CreateHotelCommandService();

            //action
            var serviceResult = await hotelCommandService.AddHotelAsync(hotelNew);

            //assert

            Assert.That(serviceResult.Succeded, Is.True);
        }



        [Test]
        public async Task CreateHotelAsync_GivenUnvalidData_ReturnServiceResultHotel()
        {
            //arrange
            var hotelNew = new HotelCreateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };

            //create command service
            var hotelCommandService = CreateHotelCommandService();

            //action
            var serviceResult = await hotelCommandService.AddHotelAsync(hotelNew);

            //assert

            Assert.That(serviceResult.Succeded, Is.False);
        }


        [Test]
        public async Task UpdateHotelAsync_GivenValidData_ReturnServiceResultHotel()
        {
            //arrange
            var hotelNew = new HotelUpdateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };
            var hotelId = new Guid();

            //create command service
            var hotelCommandService = CreateHotelCommandService();

            //action
            var serviceResult = await hotelCommandService.UpdateHotelAsync(hotelId, hotelNew);

            //assert

            Assert.That(serviceResult.Succeded, Is.True);
        }



        [Test]
        public async Task UpdateHotelAsync_GivenUnvalidData_ReturnServiceResultHotel()
        {
            //arrange
            var hotelNew = new HotelCreateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };

            //create command service
            var hotelCommandService = CreateHotelCommandService();

            //action
            var serviceResult = await hotelCommandService.AddHotelAsync(hotelNew);

            //assert

            Assert.That(serviceResult.Succeded, Is.False);
        }
    }
    
}
