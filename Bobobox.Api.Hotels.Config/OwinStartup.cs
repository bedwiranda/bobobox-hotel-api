﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Bobobox.Api.Hotels.Config.DependencyResolutions;
using Bobobox.Api.Hotels.Core.Utils;
using Bobobox.Api.Hotels.Service.DataImplementations;
using Bobobox.Api.Hotels.Versionings;

[assembly: OwinStartup(typeof(Bobobox.Api.Hotels.Config.OwinStartup))]
namespace Bobobox.Api.Hotels.Config
{
    public class OwinStartup
    {
        public OwinStartup()
        {
            //set current api version
            VersionConst.ApiLatestVersion = 4;
        }

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            var container = DependencyConfig.ConfigureDependencies(config);
            
            //AppStartupConfig.RegisterWebApi(config);
            
            var loggerService = container.GetInstance<ILoggerService>();

            //AppStartupConfig.ConfigureUtilsAndConsts(config, loggerService);
            using (var db = new AppDbContext())
            {
                //create database and update to latest
                db.Database.Initialize(false);
            }

            AppStartupConfig.RegisterWebApi(config);
            app.UseWebApi(config);

        }
    }
}
