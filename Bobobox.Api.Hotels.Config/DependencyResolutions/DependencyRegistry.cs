﻿using Bobobox.Api.Hotels.Config.Injectors;
using Bobobox.Api.Hotels.Core.DataInterfaces;
using Bobobox.Api.Hotels.Core.Shares;
using Bobobox.Api.Hotels.Core.Utils;
using Bobobox.Api.Hotels.Cores.Services.Reservations;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using Bobobox.Api.Hotels.Service.DataImplementations;
using Bobobox.Api.Hotels.Service.Services.Hotels;
using Bobobox.Api.Hotels.Service.Services.Reservations;
using Bobobox.Api.Hotels.Service.Utils;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bobobox.Api.Hotels.Config.DependencyResolutions
{
    public static class DependencyRegistry
    {
        private static Container _container;
        private static IInjectorService _injectorService;

        public static void Register(Container container)
        {
            _container = container;
            _injectorService = new InjectorService(container);

            container.RegisterInstance<IInjectorService>(_injectorService);

            RegisterCore(container);
            RegisterMain(container);
        }

        private static void RegisterCore(Container container)
        {

            container.RegisterSingleton<ILoggerService, LoggerService>();
            container.Register<IAppDbContext>(() => new AppDbContext(), Lifestyle.Scoped);
        }

        private static void RegisterMain(Container container)
        {
            //car service
            container.Register<IHotelCommandService, HotelCommandService>(
                Lifestyle.Scoped);
            container.Register<IHotelValidatorService, HotelValidatorService>(
                Lifestyle.Scoped);

            //sales service
            container.Register<IReservationCommandService, ReservationCommandService>(
                Lifestyle.Scoped);
            container.Register<IReservationValidatorService, ReservationValidatorService>(
                Lifestyle.Scoped);
        }
    }
}
