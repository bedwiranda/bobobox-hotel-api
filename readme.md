# ANSWER FOR QUESTION 1

The project structure I use is called Onion Architecture. This structure states that the inner layer does not have power to access the outer layer. And only the outer layer can access the inner one. We can check on the project reference for each projects, and we should not reference the inner layer to outer layer.

In my project structure, I added a number in each folder solution. The inner layer marks by the lower number. So Core is the most inner layer, and Test is the most outlet layer. This architecture provides a better way to build applications for better testability, maintainability, and dependability on the infrastructures like databases and services.

The structure part of the projects:
- 1. Core
This is the project (module) which comprises the base/core object, class, or interface that can be used by the whole system.
This project has the definition of interfaces, the entities, and also the objects.

- 2. Infrastructure
This project consist of the implementation of the interfaces. All of the services will be put here. This project can only access the core project.

- 3. Api
This project consists of the controllers, models, and some helpers. This project will depend only to the Core interfaces.

- 4. Config
This project will handle all of the configuration for the app, like OWIN, Initial configuration, database migration, dependency resolver, etc. This project has access to Api, Infrastructure, and Core.

- 5. Test
This project is for unit testing part. It can access whole of the other projects.


And for the code it self, I implement some of the S.O.L.I.D design principle, which are:
- Single Responsibility Principle

This principle states that a class should not be loaded with multiple responsibilities and a single responsibility should not be spread across multiple classes.

In my project, I implement it with 3 types of service: Command, Query, and Validator.
Command is used to create, update, or delete data. Query is used to retrieve data and present it to user. Validator is used to validate the on process data.

- Open Closed Principle
This principle suggests that the class should be easily extended but there is no need to change its core implementations.
In the project, we use this principle by adding the new method in an interface if needed.

- Liskov Substitution Principle
LSP states that the child class should be perfectly substitutable for their parent class. If class C is derived from P then C should be substitutable for P.
For this project, I do not use this principle yet because the project is not too complex.

- Interface Segregation Principle
Using ISP, we can create separate interfaces for each operation or requirement rather than having a single class to do the same work. This is closer to Principle 1.

- Dependency Inversion Principle
The principle says that high-level modules should depend on abstraction, not on the details, of low-level modules. In simple words, the principle says that there should not be a tight coupling among components of software and to avoid that, the components should depend on abstraction.

In the project, we use constructor injection. The high-level module (Our controllers), will depend on the the interface (abstraction), not the implementation.
