﻿using Bobobox.Api.Hotels.Configs;
using Bobobox.Api.Hotels.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Bobobox.Api.Hotels
{
    public class AppStartupConfig
    {
        public static void RegisterWebApi(HttpConfiguration config)
        {
            WebApiConfig.Register(config);
        }

        public static void ConfigureUtilsAndConsts(HttpConfiguration config,
            ILoggerService loggerService)
        {
            LoggerConfig.Configure(loggerService);
        }
    }
}