﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bobobox.Api.Hotels.Models.Hotels
{
    public class HotelEntryVM
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }
}