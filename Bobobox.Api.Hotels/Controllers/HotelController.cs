﻿using Bobobox.Api.Hotels.Core.DTO.Hotels;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using Bobobox.Api.Hotels.Models.Hotels;
using Bobobox.Api.Hotels.Versionings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bobobox.Api.Hotels.Controllers
{
    [RoutePrefix("hotels")]
    public class HotelController : ApiController
    {
        private readonly IHotelCommandService _commandService;

        public HotelController(
            IHotelCommandService commandService)
        {
            _commandService = commandService;
        }


        /// <summary>
        ///     Create Hotel
        /// </summary>
        /// <remarks>Create Hotel</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(bool))]
        [HttpPost]
        public async Task<IHttpActionResult> AddHotelAsync(
            [FromBody] HotelCreateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new HotelCreateDTO()
            {
                Name = data.Name,
                Phone = data.Phone,
                Address = data.Address,
                Email = data.Email
            };

            var result =
                await _commandService.AddHotelAsync(dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }


        /// <summary>
        ///     Update Hotel
        /// </summary>
        /// <remarks>Update Hotel</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("{hotelId:guid}")]
        [ResponseType(typeof(bool))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateHotelAsync(Guid hotelId,
            [FromBody] HotelUpdateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new HotelUpdateDTO()
            {
                Name = data.Name,
                Phone = data.Phone,
                Address = data.Address,
                Email = data.Email
            };

            var result =
                await _commandService.UpdateHotelAsync(hotelId, dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }
    }
}
