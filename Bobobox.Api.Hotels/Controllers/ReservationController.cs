﻿using Bobobox.Api.Hotels.Cores.Services.Reservations;
using Bobobox.Api.Hotels.Cores.Services.Sales;
using Bobobox.Api.Hotels.Models.Reservations;
using Bobobox.Api.Hotels.Versionings;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bobobox.Api.Hotels.Controllers
{
    [RoutePrefix("reservation")]
    public class ReservationController : ApiController
    {
        private readonly IReservationCommandService _commandService;

        public ReservationController(
            IReservationCommandService commandService)
        {
            _commandService = commandService;
        }


        /// <summary>
        ///     Create Sales
        /// </summary>
        /// <remarks>Create Sales</remarks>
        /// <param name="data">Car Data Ids</param>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(bool))]
        [HttpPost]
        public async Task<IHttpActionResult> AddSalesAsync(
            [FromBody] ReservationEntryVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration
          
            var result =
                await _commandService.CreateReservationAsync(data);

            if (result.Succeded)
            {
                return Ok(true);
            }

            return Ok(false);
        }
    }
}
